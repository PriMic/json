﻿namespace PriMicJsonManualTest.Datas
{
  public class Personne
  {
    public Personne(string nom, StringArray prenoms, int age, bool masculin)
    {
      this.nom      = nom;
      this.prenoms  = prenoms;
      this.age      = age;
      this.masculin = masculin;
    }
    public string      Nom      { get { return this.nom;      } }
    public StringArray Prenoms  { get { return this.prenoms;  } }
    public int         Age      { get { return this.age;      } }
    public bool        Masculin { get { return this.masculin; } }

    public override string ToString()
    {
      string str = this.nom + " " + this.prenoms + " " + this.age + " ans " + (this.masculin ? "garçon" : "fille") + " ";
      return str;
    }
    string      nom;
    StringArray prenoms;
    int         age;
    bool        masculin;
  }
}
