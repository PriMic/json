﻿using System.Collections.Generic;
using System.Collections;

namespace PriMicJsonManualTest.Datas
{
  public class StringArray : IEnumerable<string>
  {
    public StringArray()
    {
      this.list = new List<string>();
    }
    public void Add(string str)
    {
      this.list.Add(str);
    }
    public int Count() { return this.list.Count; }

    public IEnumerator<string> GetEnumerator() { return this.list.GetEnumerator(); }
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    public override string ToString()
    {
      string str = ""; foreach (string s in this.list) str += (s + " ");
      return str;
    }
    List<string> list;
  }
}
