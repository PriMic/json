﻿namespace PriMicJsonManualTest.Datas
{
  public class Famille
  {
    public Famille(Personne papa, Personne maman, PersonneArray enfants)
    {
      this.papa    = papa;
      this.maman   = maman;
      this.enfants = enfants;
    }
    public Personne      Papa    { get { return this.papa;    } }
    public Personne      Maman   { get { return this.maman;   } }
    public PersonneArray Enfants { get { return this.enfants; } }

    public override string ToString()
    {
      string str = "Papa:  "       + this.papa    + "\r\n"
                 + "Maman: "       + this.maman   + "\r\n"
                 + "Enfants: \r\n" + this.enfants + "\r\n";
      return str;
    }
    Personne      papa;
    Personne      maman;
    PersonneArray enfants;
  }
}
