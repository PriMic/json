﻿using System.Collections.Generic;
using System.Collections;

namespace PriMicJsonManualTest.Datas
{
  public class PersonneArray : IEnumerable<Personne>
  {
    public PersonneArray()
    {
      this.list = new List<Personne>();
    }
    public void Add(Personne personne)
    {
      this.list.Add(personne);
    }
    public int Count() { return this.list.Count; }

    public IEnumerator<Personne> GetEnumerator() { return this.list.GetEnumerator(); }
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    public override string ToString()
    {
      string str = ""; foreach (Personne p in this.list) str += (p + "\r\n");
      return str;
    }
    List<Personne> list;
  }
}
