﻿using PriMicJson;

namespace PriMicJsonManualTest.Datas.SerialisationJson
{
  public class PersonneArray
  {
    /// @brief "PersonneArray" to Json-Value.
    public static Value ToJson(Datas.PersonneArray pa)
    {
      ValueArray value = new ValueArray();
      foreach (Datas.Personne p in pa) value.Append(Personne.ToJson(p));
      return value;
    }
    /// @brief "PersonneArray" to Json-String.
    public static string ToJsonStr(Datas.PersonneArray pa)
    {
      return Json.Writer(ToJson(pa));
    }
    public static string ToJsonExtStr(Datas.PersonneArray pa)
    {
      return Json.WriterExt(ToJson(pa));
    }
    /// @brief Json-Value to "PersonneArray".
    public static Datas.PersonneArray FromJson(Value v)
    {
      Datas.PersonneArray pa = new Datas.PersonneArray();
      for (ArrayIndex i = 0; i < v.Size(); i++) pa.Add(Personne.FromJson(v[i]));
      return pa;
    }
    /// @brief Json-String to "PersonneArray".
    public static Datas.PersonneArray FromJsonStr(string s)
    {
      return FromJson(Json.Reader(s));
    }
  }
}
