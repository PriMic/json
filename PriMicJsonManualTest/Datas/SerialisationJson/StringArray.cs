﻿using PriMicJson;

namespace PriMicJsonManualTest.Datas.SerialisationJson
{
  public class StringArray
  {
    /// @brief "StringArray" to Json-Value.
    public static Value ToJson(Datas.StringArray sa)
    {
      ValueArray value = new ValueArray();
      foreach (string s in sa) value.Append(s);
      return value;
    }

    /// @brief "StringArray" to Json-String.
    public static string ToJsonStr(Datas.StringArray sa)
    {
      return Json.Writer(ToJson(sa));
    }
    public static string ToJsonExtStr(Datas.StringArray sa)
    {
      return Json.WriterExt(ToJson(sa));
    }

    /// @brief Json-Value to "StringArray".
    public static Datas.StringArray FromJson(Value v)
    {
      Datas.StringArray sa = new Datas.StringArray();
      for (ArrayIndex i = 0; i < v.Size(); i++) sa.Add(v[i].AsString());
      return sa;
    }

    /// @brief Json-String to "StringArray".
    public static Datas.StringArray FromJsonStr(string s)
    {
      return FromJson(Json.Reader(s));
    }
  }
}
