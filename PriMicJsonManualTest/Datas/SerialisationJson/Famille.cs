﻿using PriMicJson;

namespace PriMicJsonManualTest.Datas.SerialisationJson
{
  class Famille
  {
    private const string keyPapa    = "papa";
    private const string keyMaman   = "maman";
    private const string keyEnfants = "enfants";

    /// @brief "Famille" to Json-Value.
    public static Value ToJson(Datas.Famille f)
    {
      ValueObject value = new ValueObject();
      value[keyPapa   ] =      Personne.ToJson(f.Papa);
      value[keyMaman  ] =      Personne.ToJson(f.Maman);
      value[keyEnfants] = PersonneArray.ToJson(f.Enfants);
      return value;
    }
    /// @brief "Famille" to Json-String.
    public static string ToJsonStr(Datas.Famille f)
    {
      return Json.Writer(ToJson(f));
    }
    public static string ToJsonExtStr(Datas.Famille f)
    {
      return Json.WriterExt(ToJson(f));
    }
    /// @brief Json-Value to "Famille".
    public static Datas.Famille FromJson(Value v)
    {
      Datas.Personne      papa    =      Personne.FromJson(v[keyPapa   ]);
      Datas.Personne      maman   =      Personne.FromJson(v[keyMaman  ]);
      Datas.PersonneArray enfants = PersonneArray.FromJson(v[keyEnfants]);
      return new Datas.Famille(papa, maman, enfants);
    }
    /// @brief Json-String to "Famille".
    public static Datas.Famille FromJsonStr(string s)
    {
      return FromJson(Json.Reader(s));
    }
  }
}
