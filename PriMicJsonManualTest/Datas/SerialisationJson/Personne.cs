﻿using PriMicJson;

namespace PriMicJsonManualTest.Datas.SerialisationJson
{
  public class Personne
  {
    private const string keyNom      = "nom";
    private const string keyPrenoms  = "prenoms";
    private const string keyAge      = "age";
    private const string keyMasculin = "masculin";

    /// @brief "Personne" to Json-Value.
    public static Value ToJson(Datas.Personne p)
    {
      ValueObject value = new ValueObject();
      value[keyNom     ] =                    p.Nom;
      value[keyPrenoms ] = StringArray.ToJson(p.Prenoms);
      value[keyAge     ] =                    p.Age;
      value[keyMasculin] =                    p.Masculin;
      return value;
    }
    /// @brief "Personne" to Json-String.
    public static string ToJsonStr(Datas.Personne p)
    {
      return Json.Writer(ToJson(p));
    }
    public static string ToJsonExtStr(Datas.Personne p)
    {
      return Json.WriterExt(ToJson(p));
    }
    /// @brief Json-Value to "Personne".
    public static Datas.Personne FromJson(Value v)
    {
      string            nom      =                      v[keyNom     ].AsString();
      Datas.StringArray prenoms  = StringArray.FromJson(v[keyPrenoms ]);
      int               age      =                 (int)v[keyAge     ].AsLong();
      bool              masculin =                      v[keyMasculin].AsBoolean();
      return new Datas.Personne(nom, prenoms, age, masculin);
    }
    /// @brief Json-String to "Personne".
    public static Datas.Personne FromJsonStr(string s)
    {
      return FromJson(Json.Reader(s));
    }
  }
}
