﻿#define Test_Var_To_Json
#define Test_Json_To_Var

using System;

namespace PriMicJsonManualTest
{
  class Program
  {
    static void Main(string[] args)
    {
      // La famille pour les tests.
      FamilleDupond familleDupond = new FamilleDupond();

#region Json: Test_Var_To_Json.
#if           Test_Var_To_Json

      Console.WriteLine("StringArray:      " + Datas.SerialisationJson.StringArray.ToJsonStr(familleDupond.enfant1.Prenoms));
      Console.WriteLine("Personne Papa:    " + Datas.SerialisationJson.Personne.ToJsonStr(familleDupond.papa));
      Console.WriteLine("Personne Enfant1: " + Datas.SerialisationJson.Personne.ToJsonStr(familleDupond.enfant1));
      Console.WriteLine();
      Console.WriteLine("PersonneArray:    " + Datas.SerialisationJson.PersonneArray.ToJsonStr(familleDupond.famille.Enfants));
      Console.WriteLine();
      Console.WriteLine("Famille:          " + Datas.SerialisationJson.Famille.ToJsonStr(familleDupond.famille));
      Console.WriteLine();

      Console.WriteLine("StringArray:");      Console.WriteLine(Datas.SerialisationJson.StringArray.ToJsonExtStr(familleDupond.enfant1.Prenoms));   Console.WriteLine();
      Console.WriteLine("Personne Papa:");    Console.WriteLine(Datas.SerialisationJson.Personne.ToJsonExtStr(familleDupond.papa));                 Console.WriteLine();
      Console.WriteLine("Personne Enfant1:"); Console.WriteLine(Datas.SerialisationJson.Personne.ToJsonExtStr(familleDupond.enfant1));              Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine("PersonneArray:");    Console.WriteLine(Datas.SerialisationJson.PersonneArray.ToJsonExtStr(familleDupond.famille.Enfants)); Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine("Famille:");          Console.WriteLine(Datas.SerialisationJson.Famille.ToJsonExtStr(familleDupond.famille));               Console.WriteLine();
      Console.WriteLine();


#endif
      #endregion // -------------------------------------------------------------------------------------------------------------------------------------------------

      #region Json: Test_Json_To_Var.
#if Test_Json_To_Var

      Console.WriteLine();
      {
        string StringArrayJson = "[\"Jean\",\"Pierre\",\"Robert\"]";
        try
        {
          Datas.StringArray stringArray = Datas.SerialisationJson.StringArray.FromJsonStr(StringArrayJson);
          Console.WriteLine(stringArray);
        }
        catch (OverflowException oEx)
        {
          Console.WriteLine("!! Overflow Exception !!");
          Console.WriteLine(oEx.Message);
        }
        catch (ArgumentException aEx)
        {
          Console.WriteLine("!! Argument Exception !!");
          Console.WriteLine(aEx.Message);
        }
        Console.WriteLine();
      }

      {
        string PersonneJson
          = "  {                                                       "
          + "    \"nom\"      : \"Dupond\",                            "
          + "    \"prenoms\"  : [\"Luc\"],                             "
          + "    \"age\"      : 50,                                    "
          + "    \"masculin\" : true                                   "
          + "  }                                                       ";
        try
        {
          Datas.Personne personne = Datas.SerialisationJson.Personne.FromJsonStr(PersonneJson);
          Console.WriteLine(personne);
        }
        catch (OverflowException oEx)
        {
          Console.WriteLine("!! Overflow Exception !!");
          Console.WriteLine(oEx.Message);
        }
        catch (ArgumentException aEx)
        {
          Console.WriteLine("!! Argument Exception !!");
          Console.WriteLine(aEx.Message);
        }
        Console.WriteLine();
      }

      {
        string PersonneArrayJson
        = "  [                                                       "
        + "    {                                                     "
        + "      \"nom\"      : \"Dupond\",                          "
        + "      \"prenoms\"  : [\"Jean\",\"Pierre\",\"Robert\"],    "
        + "      \"age\"      : 13,                                  "
        + "      \"masculin\" : true                                 "
        + "    },                                                    "
        + "    {                                                     "
        + "      \"nom\"      : \"Dupond\",                          "
        + "      \"prenoms\"  : [\"Marie\",\"Hélène\"],              "
        + "      \"age\"      : 10,                                  "
        + "      \"masculin\" : false                                "
        + "    },                                                    "
        + "    {                                                     "
        + "      \"nom\"      : \"Dupond\",                          "
        + "      \"prenoms\"  : [\"Sophie\"],                        "
        + "      \"age\"      : 5,                                   "
        + "      \"masculin\" : false                                "
        + "    }                                                     "
        + "  ]                                                       ";
        try
        {
          Datas.PersonneArray personneArray = Datas.SerialisationJson.PersonneArray.FromJsonStr(PersonneArrayJson);
          Console.Write(personneArray);
        }
        catch (OverflowException oEx)
        {
          Console.WriteLine("!! Overflow Exception !!");
          Console.WriteLine(oEx.Message);
        }
        catch (ArgumentException aEx)
        {
          Console.WriteLine("!! Argument Exception !!");
          Console.WriteLine(aEx.Message);
        }
        Console.WriteLine();
      }

      {
        string familleJson
          = "{                                                         "
          + "  \"papa\":                                               "
          + "  {                                                       "
          + "    \"nom\"      : \"Dupond\",                            "
          + "    \"prenoms\"  : [\"Luc\"],                             "
          + "    \"age\"      : 50,                                    "
          + "    \"masculin\" : true                                   "
          + "  },                                                      "
          + "  \"maman\":                                              "
          + "  {                                                       "
          + "    \"nom\"      : \"Dupond\",                            "
          + "    \"prenoms\"  : [\"Léa\"],                             "
          + "    \"age\"      : 46,                                    "
          + "    \"masculin\" : false                                  "
          + "  },                                                      "
          + "  \"enfants\":                                            "
          + "  [                                                       "
          + "    {                                                     "
          + "      \"nom\"      : \"Dupond\",                          "
          + "      \"prenoms\"  : [\"Jean\",\"Pierre\",\"Robert\"],    "
          + "      \"age\"      : 13,                                  "
          + "      \"masculin\" : true                                 "
          + "    },                                                    "
          + "    {                                                     "
          + "      \"nom\"      : \"Dupond\",                          "
          + "      \"prenoms\"  : [\"Marie\",\"Hélène\"],              "
          + "      \"age\"      : 10,                                  "
          + "      \"masculin\" : false                                "
          + "    },                                                    "
          + "    {                                                     "
          + "      \"nom\"      : \"Dupond\",                          "
          + "      \"prenoms\"  : [\"Sophie\"],                        "
          + "      \"age\"      : 5,                                   "
          + "      \"masculin\" : false                                "
          + "    }                                                     "
          + "  ]                                                       "
          + "}                                                         ";

        try
        {
          Datas.Famille famille = Datas.SerialisationJson.Famille.FromJsonStr(familleJson);
          Console.Write(famille);
        }
        catch (OverflowException oEx)
        {
          Console.WriteLine("!! Overflow Exception !!");
          Console.WriteLine(oEx.Message);
        }
        catch (ArgumentException aEx)
        {
          Console.WriteLine("!! Argument Exception !!");
          Console.WriteLine(aEx.Message);
        }
      }
#endif
      #endregion // -------------------------------------------------------------------------------------------------------------------------------------------------
    }
  }
}
