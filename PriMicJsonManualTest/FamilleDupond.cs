﻿namespace PriMicJsonManualTest
{
  class FamilleDupond
  {
    public FamilleDupond()
    {
      Datas.StringArray papaPrenoms = new Datas.StringArray();
      papaPrenoms.Add("Luc");
      this.papa = new Datas.Personne("Dupond", papaPrenoms, 50, true);

      Datas.StringArray mamanPrenoms = new Datas.StringArray();
      mamanPrenoms.Add("Léa");
      this.maman = new Datas.Personne("Dupond", mamanPrenoms, 46, false);

      Datas.StringArray enfant1Prenoms = new Datas.StringArray();
      enfant1Prenoms.Add("Jean");
      enfant1Prenoms.Add("Pierre");
      enfant1Prenoms.Add("Robert");
      this.enfant1 = new Datas.Personne("Dupond", enfant1Prenoms, 13, true);

      Datas.StringArray enfant2Prenoms = new Datas.StringArray();
      enfant2Prenoms.Add("Marie");
      enfant2Prenoms.Add("Hélène");
      this.enfant2 = new Datas.Personne("Dupond", enfant2Prenoms, 10, false);

      Datas.StringArray enfant3Prenoms = new Datas.StringArray();
      enfant3Prenoms.Add("Sophie");
      this.enfant3 = new Datas.Personne("Dupond", enfant3Prenoms, 5, false);

      Datas.PersonneArray enfants = new Datas.PersonneArray();
      enfants.Add(this.enfant1);
      enfants.Add(this.enfant2);
      enfants.Add(this.enfant3);

      this.famille = new Datas.Famille(this.papa, this.maman, enfants);
    }

    public override System.String ToString() { return famille.ToString(); }

    public Datas.Personne papa;
    public Datas.Personne maman;
    public Datas.Personne enfant1;
    public Datas.Personne enfant2;
    public Datas.Personne enfant3;
    public Datas.Famille  famille;
  }
}
