﻿namespace PriMicJson
{
  public struct ArrayIndex
  {
    private ArrayIndex(int value) { this.value = value; }
    public static implicit operator ArrayIndex(int value) { return new ArrayIndex(value); }
    public static implicit operator int(ArrayIndex ai) { return ai.value; }
    private int value;
  }
}
