﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace PriMicJson
{
  public class ValueArray : Value, IEnumerable<Value>
  {
    public ValueArray() { this.val = new List<Value>(); }

    public void Append(Value v) { this.val.Add(v); }

    public override int Size() { return this.val.Count; }
    public override Value this[ArrayIndex i] { get { return this.val[i]; } }

    public IEnumerator<Value> GetEnumerator() { return this.val.GetEnumerator(); }
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    internal override String AsJson()
    {
      List<string> ls = new List<string>();
      foreach (var e in this.val) { ls.Add(e.AsJson()); }
      return "[" + String.Join(",", ls) + "]";
    }
    internal override String AsJson(string tabStr)
    {
      List<string> ls = new List<string>();
      string newTabStr = tabStr + "  ";
      foreach (var e in this.val) { ls.Add(e.AsJson(newTabStr)); }
      return "[" + newTabStr + String.Join("," + newTabStr, ls) + tabStr + "]";
    }

    private List<Value> val;
  }
}
