﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace PriMicJson
{
  public class ValueObject : Value, IEnumerable<KeyValuePair<string, Value>>
  {
    public ValueObject() { this.val = new Dictionary<string, Value>(); }

    public override Value this[string s]
    {
      set { this.val[s] = value;                                        }
      get { return this.val.ContainsKey(s) ? this.val[s] : new Value(); }
    }

    public IEnumerator<KeyValuePair<string, Value>> GetEnumerator() { return this.val.GetEnumerator(); }
    IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

    internal override String AsJson()
    {
      List<string> ls = new List<string>();
      foreach (var kv in this.val) ls.Add("\"" + kv.Key + "\":" + kv.Value.AsJson());
      return "{" + String.Join(",", ls) + "}";
    }
    internal override String AsJson(string tabStr)
    {
      List<string> ls = new List<string>();
      string newTabStr = tabStr + "  ";
      foreach (var kv in this.val) ls.Add("\"" + kv.Key + "\": " + kv.Value.AsJson(newTabStr));
      return "{" + newTabStr + String.Join("," + newTabStr, ls) + tabStr + "}";
    }

    private Dictionary<string, Value> val;
  }
}
