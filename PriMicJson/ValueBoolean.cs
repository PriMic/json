﻿namespace PriMicJson
{
  public class ValueBoolean : Value
  {
    public ValueBoolean(bool b) { this.val = b; }

    public override bool AsBoolean() { return this.val; }

    internal override string AsJson() { return this.val ? "true" : "false"; }
    internal override string AsJson(string tabStrb) { return AsJson(); }

    private bool val;
  }
}
