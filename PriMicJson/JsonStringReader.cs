﻿using System;

namespace PriMicJson
{
  internal class JsonStringReader
  {
    internal JsonStringReader(string s)
    {
      this.str = s;
      this.idx = -1; NextJsonCar();
    }
    internal int  Idx { get { return this.idx; } }
    internal char Car { get { return this.car; } }

    internal char NextCar() { this.idx++; this.car = this.idx < this.str.Length ? this.str[this.idx] : '\0'; return this.car; }

    internal void NextJsonCar()   { do NextCar(); while (this.car == ' ' || this.car == '\r' || this.car == '\n');           }
    private  void ReatchJsonCar() {               while (this.car == ' ' || this.car == '\r' || this.car == '\n') NextCar(); }

    internal string String()
    {
      int tag = this.idx + 1;
      do NextCar(); while (this.car != '"' && this.car != '\0');
      if (this.car == '\0') throw new ArgumentException("unexpected reaches the end of the string.");
      string s = str.Substring(tag, this.idx - tag);
      NextJsonCar();
      return s;
    }
    internal long PosInteger()
    {
      long l = (long)(this.car - '0');
      NextCar();
      while (this.Car >= '0' && this.Car <= '9')
      {
        checked { l = 10 * l + (long)(this.car - '0'); }
        NextCar();
      }
      ReatchJsonCar();
      return l;
    }
    internal long NegInteger()
    {
      long l = -(long)(this.car - '0');
      NextCar();
      while (this.Car >= '0' && this.Car <= '9')
      {
        checked { l = 10 * l - (long)(this.car - '0'); }
        NextCar();
      }
      ReatchJsonCar();
      return l;
    }
    internal double Decimal()
    {
      double d = 0, mul = 1;
      do
      {
        checked { mul /= 10; d += mul * ((double)(this.car - '0')); }
        NextCar();
      }
      while (this.Car >= '0' && this.Car <= '9');
      ReatchJsonCar();
      return d;
    }

    private string str; // Le string à lire.
    private int    idx; // L'index du caractère courant.
    private char   car; // Le caractère courant.
  }
}
