﻿namespace PriMicJson
{
  public class ValueString : Value
  {
    public ValueString(string s) { this.val = s; }

    public override string AsString() { return this.val; }

    internal override string AsJson() { return "\"" + this.val + "\""; }
    internal override string AsJson(string tabStr) { return AsJson(); }

    private string val;
  }
}
