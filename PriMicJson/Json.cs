﻿/**
 * This library proposes a Json Writer and a Json Reader.
 * @copyright  Copyright (c) 2020-2024 Michel PRINCE
 * @license    http://opensource.org/licenses/AGPL-3.0 AGPL-3.0
 * @link       https://bitbucket.org/PriMic/json/src/master/
 */

using System;

namespace PriMicJson
{
  public partial class Json
  {
    public static string Writer(Value v)
    {
      return v.AsJson();
    }
    public static string WriterExt(Value v)
    {
      return v.AsJson("\n");
    }
    public static Value Reader(string s)
    {
      return ReadValue(new JsonStringReader(s));
    }

    #region private functions

    private static string ErrorMessage(int pos, char c) { return "Error at position " + pos + ", unexpected char '" + c + "'"; }

    internal static System.Globalization.CultureInfo cultures = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

    private static Value ReadValue(JsonStringReader sr)
    {
      switch (sr.Car)
      {
        case '"': return ReadStringValue(sr);
        case '{': return ReadObjectValue(sr);
        case '[': return ReadArrayValue(sr);
        case 't': return ReadTrueValue(sr);
        case 'f': return ReadFalseValue(sr);
        case 'n': return ReadNullValue(sr);
        case '\0': return new Value();
        default : return ReadNumberValue(sr);
      }
    }
    private static ValueString ReadStringValue(JsonStringReader sr)
    {
      string s = sr.String();
      return new ValueString(s);
    }

    private static ValueNumber ReadNumberValue(JsonStringReader sr)
    {
      long entNum = 0;
      bool negNum = false;
      {
        if (sr.Car == '-') { sr.NextCar(); negNum = true; }

        if (sr.Car == '0') sr.NextCar();
        else if (sr.Car >= '1' && sr.Car <= '9')
        {
          if (negNum) entNum = sr.NegInteger();
          else        entNum = sr.PosInteger();
        }
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));
      }

      double decPart = 0;
      if (sr.Car == '.')
      {
        sr.NextCar();
        if (sr.Car >= '0' && sr.Car <= '9') decPart = sr.Decimal();
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));

        if (negNum) decPart = -decPart;
      }

      int expNum = 0;
      if (sr.Car == 'e' || sr.Car == 'E')
      {
        sr.NextCar();

        bool neg = false;
        if (sr.Car == '-') { sr.NextCar(); neg = true; }
        if (sr.Car == '+') { sr.NextCar(); }

        if (sr.Car >= '0' && sr.Car <= '9') expNum = (int)sr.PosInteger();
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));

        if (neg) expNum = -expNum;
      }
      if (decPart == 0 && expNum == 0) return new ValueLong(entNum);
      else return new ValueDouble((entNum + decPart) * Math.Pow(10, expNum));
    }

    private static ValueObject ReadObjectValue(JsonStringReader sr)
    {
      ValueObject ov = new ValueObject();
      sr.NextJsonCar();
      bool entry = sr.Car != '}';
      while (entry)
      {
        string key;
        if (sr.Car == '"') key = sr.String();
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));

        if (sr.Car == ':') sr.NextJsonCar();
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));

        ov[key] = ReadValue(sr);

        if (     sr.Car == ',') sr.NextJsonCar();
        else if (sr.Car == '}') entry = false;
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));
      }
      sr.NextJsonCar();
      return ov;
    }

    private static ValueArray ReadArrayValue(JsonStringReader sr)
    {
      ValueArray av = new ValueArray();
      sr.NextJsonCar();
      bool entry = sr.Car != ']';
      while (entry)
      {
        av.Append(ReadValue(sr));

        if      (sr.Car == ',') sr.NextJsonCar();
        else if (sr.Car == ']') entry = false;
        else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));
      }
      sr.NextJsonCar();
      return av;
    }

    private static ValueBoolean ReadTrueValue(JsonStringReader sr)
    {
      if (sr.NextCar() == 'r' && sr.NextCar() == 'u' && sr.NextCar() == 'e') sr.NextJsonCar();
      else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));
      return new ValueBoolean(true);
    }

    private static ValueBoolean ReadFalseValue(JsonStringReader sr)
    {
      if (sr.NextCar() == 'a' && sr.NextCar() == 'l' && sr.NextCar() == 's' && sr.NextCar() == 'e') sr.NextJsonCar();
      else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));
      return new ValueBoolean(false);
    }

    private static Value ReadNullValue(JsonStringReader sr)
    {
      if (sr.NextCar() == 'u' && sr.NextCar() == 'l' && sr.NextCar() == 'l') sr.NextJsonCar();
      else throw new ArgumentException(ErrorMessage(sr.Idx, sr.Car));
      return new Value();
    }
    #endregion
  }
}
