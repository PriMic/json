﻿namespace PriMicJson
{
  public class ValueNumber : Value
  {
    public ValueNumber() { }
  }
  public class ValueLong : ValueNumber
  {
    public ValueLong(long l) { this.l = l; }

    public override long   AsLong()   { return this.l; }
    public override double AsDouble() { return this.l; }

    internal override string AsJson() { return this.l.ToString(); }
    internal override string AsJson(string tabStr) { return AsJson(); }

    private long l;
  }
  public class ValueFloat : ValueNumber
  {
    public ValueFloat(float f) { this.f = f; }

    public override long   AsLong()   { return (long)this.f; }
    public override double AsDouble() { return       this.f; }

    internal override string AsJson() { return this.f.ToString(Json.cultures); }
    internal override string AsJson(string tabStr) { return AsJson(); }

    private float f;
  }
  public class ValueDouble : ValueNumber
  {
    public ValueDouble(double d) { this.d = d; }

    public override long   AsLong()   { return (long)this.d; }
    public override double AsDouble() { return       this.d; }

    internal override string AsJson() { return this.d.ToString(Json.cultures); }
    internal override string AsJson(string tabStr) { return AsJson(); }

    private double d;
  }
}
