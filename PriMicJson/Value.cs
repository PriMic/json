﻿namespace PriMicJson
{
  public class Value
  {
    public Value() { } // La classe de base "Value" représente une "value" null.

    public static implicit operator Value(string s) { return new ValueString( s); }
    public static implicit operator Value(bool   b) { return new ValueBoolean(b); }
    public static implicit operator Value(long   l) { return new ValueLong(   l); }
    public static implicit operator Value(float  f) { return new ValueFloat(  f); }
    public static implicit operator Value(double d) { return new ValueDouble( d); }

    public virtual string AsString()  { return    ""; }
    public virtual bool   AsBoolean() { return false; }
    public virtual long   AsLong()    { return     0; }
    public virtual double AsDouble()  { return     0; }

    public virtual int Size() { return 0; }
    public virtual Value this[ArrayIndex i] {         get { return new Value(); } }
    public virtual Value this[    string s] { set { } get { return new Value(); } }

    internal virtual string AsJson(             ) { return "null"; } // => Json comprimé.
    internal virtual string AsJson(string tabStr) { return "null"; } // => Json étandu.
  }
}
