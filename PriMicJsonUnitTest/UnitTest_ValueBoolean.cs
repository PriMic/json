﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueBoolean
  {
    public struct Cpl
    {
      public Cpl(string json, bool b) { this.json = json; this.b = b; }
      public string json; public bool b;
    }

    [TestMethod]
    public void Test_Boolean_Value()
    {
      Value v = true;
      Assert.IsTrue(v.AsString() == "", "Empty string expected !");
      Assert.IsTrue(v.AsBoolean(), "true value expected !");
      Assert.IsTrue(v.AsLong() == 0, "0 expected !");
      Assert.IsTrue(v.AsDouble() == 0, "0 expected !");
      Assert.IsTrue(v.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_Boolean_Value_to_Json()
    {
      Value vt = true;
      Assert.IsTrue(Json.Writer(vt) == "true", "Json 'true' expected !");
      Value vf = false;
      Assert.IsTrue(Json.Writer(vf) == "false", "Json 'false' expected !");
    }

    [TestMethod]
    public void Test_Json_to_Boolean_Value()
    {
      //                    ("  json input  ", "output expected")
      Cpl[] cpls = { new Cpl("  true xx "     , true            ),
                     new Cpl("  false x "     , false           ) };

      foreach (var cpl in cpls)
      {
        Value v = Json.Reader(cpl.json);
        Assert.IsTrue(v is ValueBoolean, "Boolean value expected from <" + cpl.json + "> !");
        Assert.IsTrue(v.AsBoolean() == cpl.b, "Unexpected boolean value !");
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Boolean_Value_Ex()
    {
      Value v = Json.Reader("  falss ");
    }
  }
}
