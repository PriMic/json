﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueString
  {
    public struct Cpl
    {
      public Cpl(string json, string s) { this.json = json; this.s = s; }
      public string json, s;
    }

    [TestMethod]
    public void Test_String_Value()
    {
      Value v = "Hello";
      Assert.IsTrue(v.AsString() == "Hello", "'Hello' string expected !");
      Assert.IsFalse(v.AsBoolean(), "false value expected !");
      Assert.IsTrue(v.AsLong() == 0, "0 expected !");
      Assert.IsTrue(v.AsDouble() == 0, "0 expected !");
      Assert.IsTrue(v.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_String_Value_to_Json()
    {
      Value v = "World";
      Assert.IsTrue(Json.Writer(v) == "\"World\"", "Json '\"World\"' expected !");
    }

    [TestMethod]
    public void Test_Json_to_String_Value()
    {
      //                    ("  json input  ", "output expected")
      Cpl[] cpls = { new Cpl("  \"\"xxx"     , ""               ),
                     new Cpl("  \"abcd\"xxx" , "abcd"           ) };

      foreach (var cpl in cpls)
      {
        Value v = Json.Reader(cpl.json);
        Assert.IsTrue(v is ValueString, "String value expected from <" + cpl.json + "> !");
        Assert.IsTrue(v.AsString() == cpl.s, "Unexpected string value !");
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_String_Value_Ex()
    {
      Value v = Json.Reader("  \"abcdx");
    }
  }
}
