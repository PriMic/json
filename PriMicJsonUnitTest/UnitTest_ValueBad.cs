﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueBad
  {
    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_To_Value()
    {
      Value v = Json.Reader("x");
    }
  }
}
