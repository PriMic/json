﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueNull
  {
    [TestMethod]
    public void Test_Null_Value()
    {
      Value v = new Value();
      Assert.IsTrue(v.AsString() == "", "Empty string expected !");
      Assert.IsFalse(v.AsBoolean(), "false value expected !");
      Assert.IsTrue(v.AsLong() == 0, "0 expected !");
      Assert.IsTrue(v.AsDouble() == 0, "0 expected !");
      Assert.IsTrue(v.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_Null_Value_to_Json()
    {
      Value v = new Value();
      Assert.IsTrue(Json.Writer(v) == "null", "Json 'null' expected !");
    }

    string[] sa = { "", "   ", " \r\n  ", "null " };

    [TestMethod]
    public void Test_Json_to_Null_Value()
    {
      foreach (string s in sa)
      {
        Value v = Json.Reader(s);
        Assert.IsFalse(v.GetType().Name != "Value", "Null value expected from <" + s + "> !");
      }
    }
  }
}
