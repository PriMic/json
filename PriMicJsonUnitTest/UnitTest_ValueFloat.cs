﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueFloat
  {
    [TestMethod]
    public void Test_Fload_Value_to_Json()
    {
      Value v = 13.005E-8F;
      Assert.IsTrue(Json.Writer(v) == "1.3005E-07", "Json '1.3005E-07' expected !");
    }
  }
}
