﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueDouble
  {
    public struct Cpl
    {
      public Cpl(string json, double d) { this.json = json; this.d = d; }
      public string json; public double d;
    }

    [TestMethod]
    public void Test_Double_Value()
    {
      Value v = -12.0045E-8D;
      Assert.IsTrue(v.AsString() == "", "Empty string expected !");
      Assert.IsFalse(v.AsBoolean(), "false value expected !");
      Assert.IsTrue(v.AsLong() == 0, "0 expected !");
      Assert.IsTrue(v.AsDouble() == -12.0045E-8D, "-12.0045E-8D expected !");
      Assert.IsTrue(v.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_Double_Value_to_Json()
    {
      Value v = -12.0045E-8D;
      Assert.IsTrue(Json.Writer(v) == "-1.20045E-07", "Json '-1.20045E-07' expected !");
    }

    [TestMethod]
    public void Test_Json_to_Double_Value()
    {
      //                    ("  json input           ", "output expected    ")
      Cpl[] cpls = { new Cpl("  0.234                " ,  0.234              ),
                     new Cpl(" -0.589                " , -0.589              ),
                     new Cpl("  12.456e2             " ,  1245.6             ),
                     new Cpl("  142536e-4            " ,  14.2536            ),
                     new Cpl("  12.456E2             " ,  1245.6             ),
                     new Cpl("  142536E-4            " ,  14.2536            ),
                     new Cpl("  12.456e20            " ,  1.2456E21          ),
                     new Cpl("  142536e-14           " ,  1.42536E-09        ),
                     new Cpl(" -12.456E20            " , -1.2456E+21         ),
                     new Cpl(" -142536E-14           " , -1.42536E-09        ),
                     new Cpl("  1234567890123456e-1  " ,  123456789012345.61 ),
                     new Cpl(" -1234567890123456e-1  " , -123456789012345.61 )};

      foreach (var cpl in cpls)
      {
        Value v = Json.Reader(cpl.json);
        Assert.IsTrue(v is ValueDouble, "Double value expected from <" + cpl.json + "> !");
        Assert.IsTrue(v.AsDouble() == cpl.d, cpl.json + " > " + v.AsDouble() + " ! " + cpl.d + " !");
      }
    }
  }
}
