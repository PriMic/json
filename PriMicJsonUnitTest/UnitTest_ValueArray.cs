﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueArray
  {
    [TestMethod]
    public void Test_Array_Value()
    {
      ValueArray tabVal = new ValueArray();
      tabVal.Append("Hello");
      tabVal.Append("World");
      Value v = tabVal;
      Assert.IsTrue(v.AsString() == "", "Empty string expected !");
      Assert.IsFalse(v.AsBoolean(), "false value expected !");
      Assert.IsTrue(v.AsLong() == 0, "0 expected !");
      Assert.IsTrue(v.AsDouble() == 0, "0 expected !");
      Assert.IsTrue(v.Size() == 2, "2 size expected !");
    }

    [TestMethod]
    public void Test_Json1_to_Array_Value()
    {
      Value vo = Json.Reader(" [ ] ");
      Assert.IsTrue(vo is ValueArray, "Array value expected !");
      Assert.IsTrue(vo.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_Json2_to_Array_Value()
    {
      Value vo = Json.Reader(" [ \"un\" ,  \"deux\" , \"trois\" ] ");
      Assert.IsTrue(vo is ValueArray, "Array value expected !");
      Assert.IsTrue(vo.Size() == 3, "3 size expected !");
      Assert.IsTrue(vo[0].AsString() == "un", "Error on firts value !");
      Assert.IsTrue(vo[1].AsString() == "deux", "Error on second value !");
      Assert.IsTrue(vo[2].AsString() == "trois", "Error on third value !");
    }
    [TestMethod]
    public void Test_Json3_to_Array_Value()
    {
      Value vo = Json.Reader(" [ 1 ,  2 , 3 ] ");
      Assert.IsTrue(vo is ValueArray, "Array value expected !");
      Assert.IsTrue(vo.Size() == 3, "3 size expected !");
      Assert.IsTrue(vo[0].AsLong() == 1, "Error on firts value !");
      Assert.IsTrue(vo[1].AsLong() == 2, "Error on second value !");
      Assert.IsTrue(vo[2].AsLong() == 3, "Error on third value !");
    }

    [TestMethod]
    public void Test_Array_Value_to_Json_to_Array_Value()
    {
      ValueArray tabVal = new ValueArray();
      tabVal.Append("Hello");
      tabVal.Append("World");
      Value vi = tabVal;

      string js = Json.Writer(vi);
      Value vo = Json.Reader(js);

      Assert.IsTrue(vo is ValueArray, "Array value expected !");
      Assert.IsTrue(vo.Size() == 2, "2 size expected !");
      Assert.IsTrue(vo[0].AsString() == "Hello", "Error on firts value !");
      Assert.IsTrue(vo[1].AsString() == "World", "Error on second value !");
    }

    [TestMethod]
    public void Test_Array_Value_to_JsonExt_to_Array_Value()
    {
      ValueArray tabVal = new ValueArray();
      tabVal.Append("Hello");
      tabVal.Append("World");
      Value vi = tabVal;

      string js = Json.WriterExt(vi);
      Value vo = Json.Reader(js);

      Assert.IsTrue(vo is ValueArray, "Array value expected !");
      Assert.IsTrue(vo.Size() == 2, "2 size expected !");
      Assert.IsTrue(vo[0].AsString() == "Hello", "Error on firts value !");
      Assert.IsTrue(vo[1].AsString() == "World", "Error on second value !");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Array_Value_Ex1()
    {
      Value v = Json.Reader(" [ x ] ");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Array_Value_Ex2()
    {
      Value v = Json.Reader(" [ 0;] ");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Array_Value_Ex3()
    {
      Value v = Json.Reader(" [ 0,] ");
    }
  }
}
