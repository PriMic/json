﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueLong
  {
    public struct Cpl
    {
      public Cpl(string json, long l) { this.json = json; this.l = l; }
      public string json; public long l;
    }

    [TestMethod]
    public void Test_Long_Value()
    {
      Value v = 7645;
      Assert.IsTrue(v.AsString() == "", "Empty string expected !");
      Assert.IsFalse(v.AsBoolean(), "false value expected !");
      Assert.IsTrue(v.AsLong() == 7645, "7645 expected !");
      Assert.IsTrue(v.AsDouble() == 7645, "7645 expected !");
      Assert.IsTrue(v.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_Long_Value_to_Json()
    {
      Value v = 7645;
      Assert.IsTrue(Json.Writer(v) == "7645", "Json '7645' expected !");
    }

    [TestMethod]
    public void Test_Json_to_Long_Value()
    {
      //                    ("  json input           ", "output expected    ")
      Cpl[] cpls = { new Cpl("  0                    " ,  0                  ),
                     new Cpl("  421                  " ,  421                ),
                     new Cpl(" -747                  " , -747                ),
                     new Cpl("  0.234                " ,  0                  ),
                     new Cpl(" -0.589                " ,  0                  ),
                     new Cpl("  12.456e2             " ,  1245               ),
                     new Cpl("  142536e-4            " ,  14                 ),
                     new Cpl("  12.456E2             " ,  1245               ),
                     new Cpl("  142536E-4            " ,  14                 ),
                     new Cpl("  9223372036854775807  " ,  9223372036854775807),
                     new Cpl(" -9223372036854775808  " , -9223372036854775808)};

      foreach (var cpl in cpls)
      {
        Value v = Json.Reader(cpl.json);
        Assert.IsTrue(v is ValueNumber, "Number value expected from <" + cpl.json + "> !");
        Assert.IsTrue(v.AsLong() == cpl.l, cpl.json + " > " + v.AsLong() + " !");
      }
    }

    [TestMethod]
    [ExpectedException(typeof(OverflowException), "Bad argument !")]
    public void Test_Json_to_Long_Value_ExMax()
    {
      Value v = Json.Reader(" 9223372036854775808 ");
    }

    [TestMethod]
    [ExpectedException(typeof(OverflowException), "Bad argument !")]
    public void Test_Json_to_Long_Value_ExMin()
    {
      Value v = Json.Reader(" -9223372036854775809 ");
    }
  }
}
