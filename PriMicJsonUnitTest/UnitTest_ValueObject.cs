﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PriMicJson;

namespace PriMicJsonUnitTest
{
  [TestClass]
  public class UnitTest_ValueObject
  {
    [TestMethod]
    public void Test_Object_Value()
    {
      ValueObject objVal = new ValueObject();
      objVal["Hello"] = "World";
      objVal["Number"] = 1024;
      Value v = objVal;
      Assert.IsTrue(v.AsString() == "", "Empty string expected !");
      Assert.IsFalse(v.AsBoolean(), "false value expected !");
      Assert.IsTrue(v.AsLong() == 0, "0 expected !");
      Assert.IsTrue(v.AsDouble() == 0, "0 expected !");
      Assert.IsTrue(v.Size() == 0, "0 size expected !");
    }

    [TestMethod]
    public void Test_Json1_to_Object_Value()
    {
      Value vo = Json.Reader(" { } ");
      Assert.IsTrue(vo is ValueObject, "Object value expected !");
    }

    [TestMethod]
    public void Test_Json2_to_Object_Value()
    {
      Value vo = Json.Reader(" { \"k1\" : \"abc\" , \"k2\" : 123 } ");
      Assert.IsTrue(vo is ValueObject, "Object value expected !");
      Assert.IsTrue(vo["k1"].AsString() == "abc", "Error in firts value !");
      Assert.IsTrue(vo["k2"].AsLong() == 123, "Error in second value !");
    }

    [TestMethod]
    public void Test_Object_Value_to_Json_to_Object_Value()
    {
      ValueObject objVal = new ValueObject();
      objVal["Hello"] = "World";
      objVal["Number"] = 1024;
      Value vi = objVal;

      string js = Json.Writer(vi);
      Value vo = Json.Reader(js);

      Assert.IsTrue(vo is ValueObject, "Object value expected !");
      Assert.IsTrue(vo["Hello"].AsString() == "World", "Error in firts value !");
      Assert.IsTrue(vo["Number"].AsLong() == 1024, "Error in second value !");
    }

    [TestMethod]
    public void Test_Object_Value_to_JsonExt_to_Object_Value()
    {
      ValueObject objVal = new ValueObject();
      objVal["Hello"] = "World";
      objVal["Number"] = 1024;
      Value vi = objVal;

      string js = Json.WriterExt(vi);
      Value vo = Json.Reader(js);

      Assert.IsTrue(vo is ValueObject, "Object value expected !");
      Assert.IsTrue(vo["Hello"].AsString() == "World", "Error in firts value !");
      Assert.IsTrue(vo["Number"].AsLong() == 1024, "Error in second value !");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Object_Value_Ex1()
    {
      Value v = Json.Reader(" {123,456} ");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Object_Value_Ex2()
    {
      Value v = Json.Reader(" {\"k1\",456} ");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Object_Value_Ex3()
    {
      Value v = Json.Reader(" {\"k1\":456x} ");
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException), "Bad argument !")]
    public void Test_Json_to_Object_Value_Ex4()
    {
      Value v = Json.Reader(" {\"k1\":456,} ");
    }
  }
}
